package com.afs.tdd;

public class Location {

    private int coordinateX;

    private int coordinateY;

    private Direction direction;

    public Location(int CoordinateX, int CoordinateY, Direction direction) {
        this.coordinateX=CoordinateX;
        this.coordinateY=CoordinateY;
        this.direction=direction;
    }
    public int getCoordinateX() {
        return coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }
}
